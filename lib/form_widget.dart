import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:pwfms/main.dart';
class FormWidget extends StatefulWidget {
  FormFieldModel formFieldModel;
  final String? imagePath;
  final bool isNumeric;


  FormWidget({
    this.imagePath,
    required this.formFieldModel,
    this.isNumeric = false,
  });

  @override
  _FormWidgetState createState() => _FormWidgetState();
}

class _FormWidgetState extends State<FormWidget> {
  File? image;

  Widget _buildChild() {
    switch (widget.formFieldModel.secondChildType) {
      case SecondChildType.text:
        return TextFormField(
          onChanged: (value) {
            widget.formFieldModel.userInput = value.toString();
          },
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "Enter ${widget.formFieldModel.headingText}",
            errorText: widget.formFieldModel.isErrorState! ? "" : null,
          ),
          keyboardType: widget.isNumeric ? TextInputType.number : TextInputType.text,
        );
      case SecondChildType.dropdown:
        if (widget.formFieldModel.dropDownOptions != null) {
          return DropdownButton(
            items: widget.formFieldModel.dropDownOptions!.map((option) => DropdownMenuItem(child: Text(option), value: option)).toList(),
            hint: Text("Select ${widget.formFieldModel.headingText}", style: TextStyle(color: widget.formFieldModel.isErrorState!?Colors.red:Colors.black)),
            value: widget.formFieldModel.userInput,
            onChanged: (value) {
              setState(() {
                widget.formFieldModel.userInput = value.toString();
              });
            },
          );
        } else {
          return Text("No options provided");
        }
      case SecondChildType.image:
        if (widget.imagePath != null) {
          return Image.file(File(widget.imagePath!));
        } else {
          return Container(
            decoration: BoxDecoration(border: Border.all(color: widget.formFieldModel.isErrorState! ?Colors.red : Colors.black)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    IconButton(onPressed: (){Fluttertoast.showToast(msg: "From gallery");}, icon: Icon(Icons.photo_album)),
                    const Text("Upload Picture"),
                  ],
                ),
                Column(
                  children: [
                    IconButton(onPressed: (){Fluttertoast.showToast(msg: "From Camera");}, icon: Icon(Icons.camera)),
                    const Text("Take Picture"),
                  ],
                ),
              ],
            ),
          );
        }
      default:
        return Text("UI Error!!", style: TextStyle(color: Colors.red));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical:10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                widget.formFieldModel.headingText!,
                style: const TextStyle(fontSize: 18),
              ),
            ],
          ),
          _buildChild(),
        ],
      ),
    );
  }

  Future<void> _pickImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(source: source);
    if (image != null) {
      setState(() {
        this.image = File(image.path);
      });
    }
  }
}