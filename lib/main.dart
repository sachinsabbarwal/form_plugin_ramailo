import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'form_widget.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Map<String, dynamic>> data = [];
  Map<String,String>? _formDataMap ;
  List<FormFieldModel> models = [];

  @override
  void initState() {
    super.initState();
    data = [
      {FormFieldModel.kKeyforHeadingText: "Take a picture",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.image
      },
      {
        FormFieldModel.kKeyforHeadingText: "Facility Manager",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.dropdown,
        FormFieldModel.kKeyforDropDownOptions: ["Prashant","Ramailo","Rahul","sachin"]
      },
      {FormFieldModel.kKeyforHeadingText: "Security",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
      {FormFieldModel.kKeyforHeadingText: "Guests",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
      {FormFieldModel.kKeyforHeadingText: "Floor Incharge",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
      {FormFieldModel.kKeyforHeadingText: "Class incharge",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
      {FormFieldModel.kKeyforHeadingText: "House Keeping",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
      {FormFieldModel.kKeyforHeadingText: "PW Members",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
      {FormFieldModel.kKeyforHeadingText: "Office Boy",
        FormFieldModel.kKeyforSecondChildType: SecondChildType.text
      },
    ];
    models = data.map((json) {
      return FormFieldModel(
        headingText: json[FormFieldModel.kKeyforHeadingText],
        secondChildType: json[FormFieldModel.kKeyforSecondChildType],
        isErrorState: false,
        dropDownOptions: json[FormFieldModel.kKeyforDropDownOptions],
        userInput: null,
      );
    }).toList();

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Demo'),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) => FormWidget(
                  formFieldModel: models[index],)
              ),
            ),
            //make tow buttons, save and cancel
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: () {
                    //save the data
                    _formDataMap = {};
                    for (int index =0;index<models.length;index++) {

                       if(models[index].userInput != null && models[index].userInput!.isNotEmpty){
                         _formDataMap![models[index].headingText!] = models[index].userInput!;
                         setState(() {
                           models[index].isErrorState = false;
                         });
                       }else{
                         setState(() {
                           models[index].isErrorState = true;
                         });
                       }
                    }
                    print("***************------------------*****************");

                    if(_formDataMap?.length == models.length){
                      //to send this map data to api
                      print(_formDataMap);
                    }else{
                      print("Full map not made");
                    }
                  },
                  child: Text("Save"),
                ),
                ElevatedButton(
                  onPressed: () {
                    //cancel the data
                    //TODO: ask are you sure?
                    Navigator.pop(context);
                  },
                  child: Text("Cancel"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

enum SecondChildType {
  text,
  dropdown,
  image,
}
class FormFieldModel{
  static const kKeyforDropDownOptions = 'drop_down_options';
  static const kKeyforHeadingText = 'heading_text';
  static const kKeyforSecondChildType = 'second_child_type';
  static const kKeyforIsErrorState = 'is_error_state';
  static const kKeyforUserInput = 'user_input';

  String? headingText;
  SecondChildType secondChildType;
  bool? isErrorState;
  String? userInput;
  List<String>? dropDownOptions;
  FormFieldModel({required  this.headingText,
    required this.secondChildType,
    required this.isErrorState,
    this.userInput,
    this.dropDownOptions});
}